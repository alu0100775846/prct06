#Practica 6 Linked List

Práctica de Laboratorio 6.

Desarrollo dirigido por pruebas (Test Driven Development, TDD).

Clase lista enlazada con nodos.

Ejemplos de definiciones de expectativas son las siguientes:
	
Exam

* Debe existir una pregunta.
* Se debe invocar a un metodo para obtener la pregunta.
* Deben existir opciones de respuesta.
* Se debe invocar a un metodo para obtener las opciones de respuesta.
* Se deben mostrar por la consola formateada la pregunta y las opciones de respuesta.


Node
* Debe existir un Nodo de la lista con sus datos y su siguiente

List
* Se extrae el primer elemento de la lista.
* Se puede insertar un elemento.
* Se pueden insertar varios elementos.
* Debe existir una Lista con su cabeza.

Realizado por:

* Juan Tareq Gonzalez de Chavez Perez

* Jose Manuel Hernandez Hernandez
